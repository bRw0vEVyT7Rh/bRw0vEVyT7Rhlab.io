var modal3D = false;

$(document).ready(function () {
  $( ".svgButton" ).click(function() {
    if (  $( this ).css( "transform" ) == 'none' ){
        $(this).css("transform","rotate(45deg)");
    } else {
        $(this).css("transform","" );
    }
});
    $("button.galleryImageContainer").on("click", function () {
      $( "#modalGLB").addClass("hide");
      $( "#modalImage").removeClass("hide");
      $( "#buttonModal3D").text("SHOW 3D").css({'color':'white','font-weight':'bold'});
      $( "#buttonModal3DMobile").text("SHOW 3D").css({'color':'white','font-weight':'bold'});

      modal3D = false;
        console.log("clicked");
        let test = $(this).attr("value");
        console.log(test);
        let values = test.split(',div0x,');
        console.log(JSON.stringify(values));
        $( "#myModal" ).removeClass( "notDisplayBlock",1000 );
        $( "#myModal" ).addClass( "displayBlock",1000);
        let location = "/assets/cards/deck_chart/" + values[0];
        let location3d = "/assets/cards/glb/" + values[6];
        $( "#modalImage").attr("src",location);
        $( "#modalTitle").text(values[1]+' - ' + values[2]);
        $( "#modalRarity").text(values[4]+'%'+' - '+values[5]);
        $( "#modalStory").html(values[3]);
        console.log("value 6");
        console.log(values[6]);
        if(values[6] == ""){
          console.log("empty");
          $( "#buttonModal3D").addClass('hide');
          $( "#buttonModal3DMobile").addClass('hide');
        }
        else {
          $( "#buttonModal3D").removeClass('hide');
          $( "#buttonModal3DMobile").removeClass('hide');
          $( "#modalGLB").attr("src",location3d);
        }
        const img = new Image();
        img.onload = function() {
          if(this.width > this.height) {
            console.log("adding class");
            $( "#modalStory").addClass("bigWidth");
            $( "#modalImage").addClass("orizzontalImage");
          }
          if(this.width < this.height) {
            console.log("removing class");
            $( "#modalStory").removeClass("bigWidth");
            $( "#modalImage").removeClass("orizzontalImage");

          }
        }
        img.src = location;
    });

    $("button.galleryImageContainerOrizzontal").on("click", function () {
      $( "#modalGLB").addClass("hide");
      $( "#modalImage").removeClass("hide");
      $( "#buttonModal3D").text("SHOW 3D").css({'color':'white','font-weight':'bold'});
      $( "#buttonModal3DMobile").text("SHOW 3D").css({'color':'white','font-weight':'bold'});
      modal3D = false;
        console.log("clicked");
        let test = $(this).attr("value");
        console.log(test);
        let values = test.split(',div0x,');
        console.log(JSON.stringify(values));
        $( "#myModal" ).removeClass( "notDisplayBlock",1000 );
        $( "#myModal" ).addClass( "displayBlock",1000);
        let location = "/assets/cards/deck_chart/" + values[0];
        let location3d = "/assets/cards/glb/" + values[6];
        $( "#modalImage").attr("src",location);
        $( "#modalTitle").text(values[1]+' - ' + values[2]);
        $( "#modalRarity").text(values[4]+'%'+' - '+values[5]);
        $( "#modalStory").html(values[3]);
        console.log("value 6");
        console.log(values[6]);
        if(values[6] == ""){
          console.log("empty");
          $( "#buttonModal3D").addClass('hide');
          $( "#buttonModal3DMobile").addClass('hide');
        }
        else {
          $( "#buttonModal3D").removeClass('hide');
          $( "#buttonModal3DMobile").removeClass('hide');
          $( "#modalGLB").attr("src",location3d);
        }
        const img = new Image();
        img.onload = function() {
          if(this.width > this.height) {
            console.log("adding class");
            $( "#modalStory").addClass("bigWidth");
            $( "#modalImage").addClass("orizzontalImage");
          }
          if(this.width < this.height) {
            console.log("removing class");
            $( "#modalStory").removeClass("bigWidth");
            $( "#modalImage").removeClass("orizzontalImage");

          }
        }
        img.src = location;
    });
    console.log("current page", window.location.href);
    $("[href]").each(function () {
        $('a[href]').each(function () {

            if (window.location.href.indexOf($(this).attr('href')) > -1) {
                if($(this).attr('href') != '/'){
                console.log($(this).attr('href') +" is active ");
                $(this).addClass('active');
                }
                else {
                  console.log("AAAAAAAAAAAAAAAAAAAAAAA");
                  console.log(window.location.href.split('/')[3]);
                  if(window.location.href.split('/')[3] === ''){
                  $(this).addClass('active');
                  }
                }
            }
            else {
                console.log($(this).attr('href') + "is not active ");
            }
        });
    });
    $("span.close").on("click", function () {
        console.log("clicked");
        $( "#myModal" ).removeClass( "displayBlock" );
        $( "#myModal" ).addClass( "notDisplayBlock",1000 );
    });

    $('#myModalContent').hover(function(){ 
      console.log("in");
      mouse_is_inside=true; 
  }, function(){ 
    console.log("out");
      mouse_is_inside=false; 
  });

  $("body").mouseup(function(){ 
    console.log("called");
      if(! mouse_is_inside) {
        console.log("not inside");
        $( "#myModal" ).removeClass( "displayBlock" );
        $( "#myModal" ).addClass( "notDisplayBlock",1000 );
      }
  });

    var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
      console.log("clicked");
    this.classList.toggle("active");
    var svg = $(this).find('svg');
    var content = this.nextElementSibling;
    console.log(this);
    if (content.style.display === "block") {
      content.style.display = "none";
    
      svg.animate({  borderSpacing: 0 }, {
        step: function(now,fx) {
          svg.css("transform",'rotate('+now+'deg)');
        },
        duration:500
    },'linear');

    } else {
      content.style.display = "block";
      svg.animate({  borderSpacing: 180 }, {
        step: function(now,fx) {
          svg.css("transform",'rotate('+now+'deg)');
        },
        duration:500
    },'linear');    }
  });
}
}
);

function toggle3d(){
  console.log("test");
  if(modal3D === false){
  $( "#modalImage").addClass("hide");
  $( "#modalGLB").removeClass("hide");
  $( "#buttonModal3D").text("SHOW 2D").css({'color':'white','font-weight':'bold'});
  $( "#buttonModal3DMobile").text("SHOW 2D").css({'color':'white','font-weight':'bold'});
  modal3D = true;
  }
  else{
    $( "#modalGLB").addClass("hide");
    $( "#modalImage").removeClass("hide");
    $( "#buttonModal3D").text("SHOW 3D").css({'color':'white','font-weight':'bold'});
    $( "#buttonModal3DMobile").text("SHOW 3D").css({'color':'white','font-weight':'bold'});
    modal3D = false;
  }
}


